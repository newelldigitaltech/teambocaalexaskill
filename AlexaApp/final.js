/* eslint-disable  func-names */
/* eslint-disable  no-restricted-syntax */
/* eslint-disable  no-loop-func */
/* eslint-disable  consistent-return */
/* eslint-disable  no-console */
/* eslint-disable max-len */
/* eslint-disable prefer-destructuring */

const Alexa = require('ask-sdk-core');
const https = require('https');

/* INTENT HANDLERS */

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
  },
  handle(handlerInput) {
    return handlerInput.responseBuilder
      .speak('Welcome to the FoodSaver Alexa skill. What can I help you find today?')
      .reprompt('What size bags or rolls are you looking for?')
      .getResponse();
  },
};

const FSIntent = {
  canHandle(handlerInput) {
      //if (handlerInput.requestEnvelope.request.type !== 'IntentRequest'
      //|| handlerInput.requestEnvelope.request.intent.name !== 'FSIntent') {
      //return false;
      //}

        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'FSIntent';
    },
    
      async handle(handlerInput) {
        //const petMatchOptions = buildPetMatchOptions('Florida');
        const fsOptions = buildFSOptions('gallon bags');
        let outputSpeech = '';
        
        try {
          //const response = await httpGet(petMatchOptions);
          //outputSpeech = 'Hello ' + response.Name + '! Your population is ' + response.population;
          const fsprods = await httpGetProducts(fsOptions);
          
          //var names = new Array;
          //for (var i = 0; i < fsprods.hits.length; i++) {
          //    names.push(fsprods.hits[i].product_name);
          //}
          //var namestring = names.join();
          
          outputSpeech = 'We recommend ' + fsprods.hits[0].product_name; // + '. Here is a link: ' + fsprods.hits[0].link;
          //console.log(fsprods);
        } catch (error) {
          outputSpeech = 'I am really sorry. I am unable to access part of my memory. Please try again later';
          console.log(`Intent: ${handlerInput.requestEnvelope.request.intent.name}: message: ${error.message}`);
        }
        return handlerInput.responseBuilder
          .speak(outputSpeech)
          .getResponse();
        }
};

const VSIntent = {
  canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'VSIntent';
    },
    
      async handle(handlerInput) {
        const fsOptions = buildFSOptions('black vacuum sealer');
        let outputSpeech = '';
        
        try {
          const fsprods = await httpGetProducts(fsOptions);
          outputSpeech = 'We recommend ' + fsprods.hits[0].product_name;
        } catch (error) {
          outputSpeech = 'I am really sorry. I am unable to access part of my memory. Please try again later';
          console.log(`Intent: ${handlerInput.requestEnvelope.request.intent.name}: message: ${error.message}`);
        }
        return handlerInput.responseBuilder
          .speak(outputSpeech)
          .getResponse();
        }
};

const PartsIntent = {
  canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest'
            && request.intent.name === 'PartsIntent';
    },
    
      async handle(handlerInput) {
        const fsOptions = buildFSOptions('gasket');
        let outputSpeech = '';
        
        try {
          const fsprods = await httpGetProducts(fsOptions);
          outputSpeech = 'We recommend ' + fsprods.hits[0].product_name;
        } catch (error) {
          outputSpeech = 'I am really sorry. I am unable to access part of my memory. Please try again later';
          console.log(`Intent: ${handlerInput.requestEnvelope.request.intent.name}: message: ${error.message}`);
        }
        return handlerInput.responseBuilder
          .speak(outputSpeech)
          .getResponse();
        }
};


const FallbackHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.FallbackIntent';
  },
  handle(handlerInput) {
    return handlerInput.responseBuilder
      .speak('I\'m sorry My FoodSaver can\'t help you with that. ' +
        'I can help you find the perfect product for you. You can say, I gallon bags.')
      .reprompt('What size or color product are you looking for?')
      .getResponse();
  },
};

const HelpHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;

    return request.type === 'IntentRequest'
      && request.intent.name === 'AMAZON.HelpIntent';
  },
  handle(handlerInput) {
    return handlerInput.responseBuilder
      .speak('This is My FoodSaver. I can help you find the perfect product for you. You can say, I gallon bags.')
      .reprompt('What size or color product are you looking for?')
      .getResponse();
  },
};

const ExitHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;

    return request.type === 'IntentRequest'
      && (request.intent.name === 'AMAZON.CancelIntent'
        || request.intent.name === 'AMAZON.StopIntent');
  },
  handle(handlerInput) {
    return handlerInput.responseBuilder
      .speak('Bye')
      .getResponse();
  },
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
  },
  handle(handlerInput) {
    console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);

    return handlerInput.responseBuilder.getResponse();
  },
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${handlerInput.requestEnvelope.request.type} ${handlerInput.requestEnvelope.request.type === 'IntentRequest' ? `intent: ${handlerInput.requestEnvelope.request.intent.name} ` : ''}${error.message}.`);

    return handlerInput.responseBuilder
      .speak('Sorry, I can\'t understand the command. Please say again.')
      .reprompt('Sorry, I can\'t understand the command. Please say again.')
      .getResponse();
  },
};


/* CONSTANTS */

const petMatchApi = {
  hostname: 'e4v7rdwl7l.execute-api.us-east-1.amazonaws.com',
  pets: '/Test',
};


function buildHttpGetOptions(host, path, port, params) {
  return {
    //hostname: host,
    //path: path + buildQueryString(params),
    hostname: 'cp6gckjt97.execute-api.us-east-1.amazonaws.com',
    path: '/prod/stateresource?usstate=' + params,
    
    port,
    method: 'GET',
  };
}

const fsApi = {
  hostname: 'dev08-web-jarden.demandware.net',
  pets: '/s/food-saver/dw/shop/v18_6/product_search?q=gallon&client_id=dbc414d6-1bd3-406b-a3c7-2e28bb110451',
};

function buildFSOptions(param) {
  const params = param; //buildPetMatchParams(slotValues);
  const port = 443;
  return buildHttpGetFSOptions(fsApi.hostname, fsApi.pets, port, encodeURIComponent(param));
}

function buildHttpGetFSOptions(host, path, port, params) {
  return {
    //hostname: host,
    //path: path + buildQueryString(params),
    hostname: 'dev08-web-jarden.demandware.net',
    path: '/s/food-saver/dw/shop/v18_6/product_search?q=' + params + '&client_id=dbc414d6-1bd3-406b-a3c7-2e28bb110451',
    port,
    method: 'GET',
  };
}


//function httpGet(options) {
//  return new Promise(((resolve, reject) => {
//    const request = https.request(options, (response) => {
//      response.setEncoding('utf8');
//      let returnData = '';

//      if (response.statusCode < 200 || response.statusCode >= 300) {
//        return reject(new Error(`${response.statusCode}: ${response.req.getHeader('host')} ${response.req.path}`));
//      }

//      response.on('data', (chunk) => {
//        returnData += chunk;
//      });

//      response.on('end', () => {
        //resolve(JSON.parse(returnData).population);
//        resolve(JSON.parse(returnData));
//      });

//      response.on('error', (error) => {
//        reject(error);
//      });
//    });
//    request.end();
//  }));
//}

function httpGetProducts(options) {
  return new Promise(((resolve, reject) => {
    const request = https.request(options, (response) => {
      response.setEncoding('utf8');
      let returnData = '';

      if (response.statusCode < 200 || response.statusCode >= 300) {
        return reject(new Error(`${response.statusCode}: ${response.req.getHeader('host')} ${response.req.path}`));
      }

      response.on('data', (chunk) => {
        returnData += chunk;
      });

      response.on('end', () => {
        //resolve(JSON.parse(returnData).population);
        resolve(JSON.parse(returnData));
      });

      response.on('error', (error) => {
        reject(error);
      });
    });
    request.end();
  }));
}

const skillBuilder = Alexa.SkillBuilders.custom();

/* LAMBDA SETUP */
exports.handler = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    //MyTypeHandler,
    FSIntent,
    VSIntent,
    PartsIntent,
    //InProgressPetMatchIntent,
    //CompletedPetMatchIntent,
    HelpHandler,
    FallbackHandler,
    ExitHandler,
    SessionEndedRequestHandler,
  )
  .addErrorHandlers(ErrorHandler)
  .lambda();